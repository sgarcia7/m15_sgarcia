package m15_2nconv.models;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table (name = "tbl_transportista")
/**
 * Clase que representa al transportista en la base de dades
 */
public class Transportista implements Serializable{

    @Id
    @Column(name = "id", unique = true)
    private int id;

    @Column(name = "login")
    private String login;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "id", updatable = false, nullable = false)
    private DadesTransportista dadesTransportista;

    public DadesTransportista getDadesTransportista() {
        return dadesTransportista;
    }

    public void setDadesTransportista(DadesTransportista dadesTransportista) {
        this.dadesTransportista = dadesTransportista;
    }

    //Constructors
    public Transportista() {
    }

    public Transportista(int id, String nom) {
        this.id = id;
        this.login = login;
    }

    //Getters and Setters


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String toString() {
        return "Transportista{" +
                "id=" + id +
                ", login=" + login +
                '}';
    }


}
