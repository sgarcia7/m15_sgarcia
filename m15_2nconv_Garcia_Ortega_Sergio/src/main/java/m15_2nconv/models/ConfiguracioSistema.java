package m15_2nconv.models;

import javax.persistence.*;

@Entity
@Table(name = "tbl_configuracio_sistema")
public class ConfiguracioSistema{

    @Id
    @Column(name = "id", unique = true)
    private int id;

    @Column(name = "passMaxIntents")
    private int passMaxIntents;

    @Column(name = "passMinCaracters")
    private int passMinCaracters;

    @Column(name = "emailSMPT")
    private String emailSMPT;

    @Column(name = "emailUser")
    private String emailUser;

    @Column(name = "emailContrasenya")
    private String emailContrasenya;

    @Column(name = "emailAbout")
    private String emailAbout;

    @Column(name = "emailBody")
    private String emailBody;

    @Column(name = "influxdbHostAdress")
    private String influxdbHostAdress;

    @Column(name = "influxdbPort")
    private String influxdbPort;

    @Column(name = "influxdbNomBBDD")
    private String influxdbNomBBDD;

    @Column(name = "influxdbUsuari")
    private String influxdbUsuari;

    @Column(name = "influxdbContrasenya")
    private String influxdbContrasenya;

    public ConfiguracioSistema() {

    }

    public int getPassMaxIntents() {
        return passMaxIntents;
    }

    public void setPassMaxIntents(int passMaxIntents) {
        this.passMaxIntents = passMaxIntents;
    }

    public int getPassMinCaracters() {
        return passMinCaracters;
    }

    public void setPassMinCaracters(int passMinCaracters) {
        this.passMinCaracters = passMinCaracters;
    }

    public String getEmailSMPT() {
        return emailSMPT;
    }

    public void setEmailSMPT(String emailSMPT) {
        this.emailSMPT = emailSMPT;
    }

    public String getEmailUser() {
        return emailUser;
    }

    public void setEmailUser(String emailUser) {
        this.emailUser = emailUser;
    }

    public String getEmailContrasenya() {
        return emailContrasenya;
    }

    public void setEmailContrasenya(String emailContrasenya) {
        this.emailContrasenya = emailContrasenya;
    }

    public String getEmailAbout() {
        return emailAbout;
    }

    public void setEmailAbout(String emailAbout) {
        this.emailAbout = emailAbout;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    public String getInfluxdbHostAdress() {
        return influxdbHostAdress;
    }

    public void setInfluxdbHostAdress(String influxdbHostAdress) {
        this.influxdbHostAdress = influxdbHostAdress;
    }

    public String getInfluxdbPort() {
        return influxdbPort;
    }

    public void setInfluxdbPort(String influxdbPort) {
        this.influxdbPort = influxdbPort;
    }

    public String getInfluxdbNomBBDD() {
        return influxdbNomBBDD;
    }

    public void setInfluxdbNomBBDD(String influxdbNomBBDD) {
        this.influxdbNomBBDD = influxdbNomBBDD;
    }

    public String getInfluxdbUsuari() {
        return influxdbUsuari;
    }

    public void setInfluxdbUsuari(String influxdbUsuari) {
        this.influxdbUsuari = influxdbUsuari;
    }

    public String getInfluxdbContrasenya() {
        return influxdbContrasenya;
    }

    public void setInfluxdbContrasenya(String influxdbContrasenya) {
        this.influxdbContrasenya = influxdbContrasenya;
    }
}
