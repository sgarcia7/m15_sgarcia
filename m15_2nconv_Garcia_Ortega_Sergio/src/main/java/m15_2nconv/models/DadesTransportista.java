package m15_2nconv.models;

import javafx.beans.property.SimpleBooleanProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;


@Entity
@Table(name = "tbl_dades_transportista")
/**
 */

public class DadesTransportista implements Serializable {

    @Id
    @Column(name = "transportista_id", unique = true)
    private int idTransportista;

    @OneToOne(mappedBy = "dadesTransportista", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)

    private Transportista transportista;

    @Column(name = "nom")
    private String nom;

    @Column(name = "email")
    private String email;

    @Column(name = "telefon")
    private String telefon;

    @Column(name = "cifnif")
    private String cifnif;

    @Column(name = "raosocial")
    private String raosocial;

    @Column(name = "datacert_hisenda")
    private LocalDate datacert_hisenda;

    @Column(name = "datacert_prl")
    private LocalDate datacert_prl;

    @Column(name = "datacert_merc")
    private LocalDate datacert_merc;

    @Column(name = "dataaseg_rc")
    private LocalDate dataaseg_rc;

    @Column(name = "trade")
    private boolean trade;

    @Column(name = "baixa")
    private boolean baixa;

    @Column(name = "pathcert_hisenda")
    private String pathcert_hisenda;


    @Column(name = "pathcert_prl")
    private String pathcert_prl;

    @Column(name = "pathcert_merc")
    private String pathcert_merc;

    @Column(name = "pathaseg_rc")
    private String pathaseg_rc;

    @Column(name = "pathtrade")
    private String pathtrade;


    public DadesTransportista() {
    }

    public int getIdTransportista() {
        return idTransportista;
    }

    public void setIdTransportista(int idTransportista) {
        this.idTransportista = idTransportista;
    }

    public Transportista getTransportista() {
        return transportista;
    }

    public boolean isTrade() {
        return trade;
    }

    public void setTrade(boolean trade) {
        this.trade = trade;
    }

    public void setTransportista(Transportista transportista) {
        this.transportista = transportista;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPathtrade() {
        return pathtrade;
    }

    public void setPathtrade(String pathtrade) {
        this.pathtrade = pathtrade;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getCifnif() {
        return cifnif;
    }

    public void setCifnif(String cifnif) {
        this.cifnif = cifnif;
    }

    public String getRaosocial() {
        return raosocial;
    }

    public void setRaosocial(String raosocial) {
        this.raosocial = raosocial;
    }

    public LocalDate getDatacert_hisenda() {
        return datacert_hisenda;
    }

    public void setDatacert_hisenda(LocalDate datacert_hisenda) {
        this.datacert_hisenda = datacert_hisenda;
    }

    public LocalDate getDatacert_prl() {
        return datacert_prl;
    }

    public void setDatacert_prl(LocalDate datacert_prl) {
        this.datacert_prl = datacert_prl;
    }

    public LocalDate getDatacert_merc() {
        return datacert_merc;
    }

    public void setDatacert_merc(LocalDate datacert_merc) {
        this.datacert_merc = datacert_merc;
    }

    public LocalDate getDataaseg_rc() {
        return dataaseg_rc;
    }

    public void setDataaseg_rc(LocalDate dataaseg_rc) {
        this.dataaseg_rc = dataaseg_rc;
    }

    public String getPathcert_hisenda() {
        return pathcert_hisenda;
    }

    public void setPathcert_hisenda(String pathcert_hisenda) {
        this.pathcert_hisenda = pathcert_hisenda;
    }

    public String getPathcert_prl() {
        return pathcert_prl;
    }

    public void setPathcert_prl(String pathcert_prl) {
        this.pathcert_prl = pathcert_prl;
    }

    public String getPathcert_merc() {
        return pathcert_merc;
    }

    public void setPathcert_merc(String pathcert_merc) {
        this.pathcert_merc = pathcert_merc;
    }

    public String getPathaseg_rc() {
        return pathaseg_rc;
    }

    public void setPathaseg_rc(String pathaseg_rc) {
        this.pathaseg_rc = pathaseg_rc;
    }

    public boolean isBaixa() {
        return baixa;
    }

    public void setBaixa(boolean baixa) {
        this.baixa = baixa;
    }

    // es necessita per omplir la casella propi el formulari de cerca
    public SimpleBooleanProperty getBixaProperty() {
        return new SimpleBooleanProperty(baixa);
    }

}
