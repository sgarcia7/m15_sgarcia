package m15_2nconv.controladors;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import m15_2nconv.classes.DAOHelper;
import m15_2nconv.classes.DefaultControllerTransportista;
import m15_2nconv.models.DadesTransportista;
import m15_2nconv.models.Transportista;
import org.apache.commons.lang.ObjectUtils;

import java.io.File;

public class AltaTransportistaController extends DefaultControllerTransportista {
    @FXML
    private AnchorPane anchorPaneAlta;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnX;
    @FXML
    private CheckBox cbTrade;
    @FXML
    private DatePicker dpCertificat;
    @FXML
    private DatePicker dpPRL;
    @FXML
    private DatePicker dpAssegMerc;
    @FXML
    private DatePicker dpAssegRC;
    @FXML
    private Button btnCertificat;
    @FXML
    private Button btnPRL;
    @FXML
    private Button btnAssegMerc;
    @FXML
    private Button btnAssegRC;
    @FXML
    private Button btnTrade;
    @FXML
    private TextField tfRaoSocial;
    @FXML
    private TextField tfCIFNIF;
    @FXML
    private TextField tfID;
    @FXML
    private TextField tfNom;
    @FXML
    private TextField tfTelef;
    @FXML
    private TextField tfEmail;
    @FXML
    private CheckBox cbBaixaTemporal;

    String certificatHisendaPath,certificatPRLPath,assegurancaMercadeiraPath,assegurancaRCPath,tradePath;

    @FXML
    public void btnSaveOnAction (ActionEvent event) {
        DAOHelper<DadesTransportista> daoHelper = new DAOHelper<>(this.getEmf(), DadesTransportista.class);
        DAOHelper<Transportista> daoHelper2 = new DAOHelper<>(this.getEmf(), Transportista.class);

        DadesTransportista dadesTransportista = new DadesTransportista();
        Transportista transportista = new Transportista();

        if (!tfRaoSocial.getText().isEmpty() || !tfCIFNIF.getText().isEmpty() || !tfEmail.getText().isEmpty()) {


            transportista.setId(Integer.parseInt(tfID.getText()));
            dadesTransportista.setIdTransportista(Integer.parseInt(tfID.getText()));

            dadesTransportista.setNom(tfNom.getText());
            dadesTransportista.setRaosocial(tfRaoSocial.getText());
            dadesTransportista.setCifnif(tfCIFNIF.getText());
            dadesTransportista.setTelefon(tfTelef.getText());
            dadesTransportista.setEmail(tfEmail.getText());

            dadesTransportista.setDatacert_hisenda(dpCertificat.getValue());
            dadesTransportista.setDatacert_prl(dpPRL.getValue());
            dadesTransportista.setDatacert_merc(dpCertificat.getValue());
            dadesTransportista.setDataaseg_rc(dpCertificat.getValue());

            dadesTransportista.setPathcert_hisenda(certificatHisendaPath);
            dadesTransportista.setPathcert_prl(certificatPRLPath);
            dadesTransportista.setPathcert_merc(assegurancaMercadeiraPath);
            dadesTransportista.setPathaseg_rc(assegurancaRCPath);
            dadesTransportista.setPathtrade(tradePath);

            dadesTransportista.setTrade(cbTrade.isSelected());



            if (cbBaixaTemporal.isSelected()) {
                dadesTransportista.setBaixa(true);
            } else {
                dadesTransportista.setBaixa(false);

            }

            if (!tfID.getText().equals(daoHelper2.getById(Integer.valueOf(tfID.getText())))) {
                daoHelper2.insert(transportista);
                daoHelper.insert(dadesTransportista);
                mostraAlertaInformation();
            } else if (tfEmail.getText().isEmpty()) {
                mostraAlertaWarning(
                        "Introdueix un email",
                        "El camp de email no pot quedar buit"
                );
            } else if (tfCIFNIF.getText().isEmpty()) {
                mostraAlertaWarning(
                        "Introdueix un CIF/NIF",
                        "El camp de CIF/NIF no pot quedar buit"
                );
            } else if (tfRaoSocial.getText().isEmpty()) {
                mostraAlertaWarning(
                        "Introdueix Rao social / Autonom",
                        "El camp de Rao social / Autonom no pot quedar buit"
                );
            } else
                mostraAlertaWarning(
                        "Introdueix una ID valida",
                        "El ID no pot ser repetit"
                );
        }

          }
    @FXML
    public void cbTradeActvivar (ActionEvent event) {
        if(cbTrade.isSelected()){
            btnTrade.setDisable(false);
        }else {
            btnTrade.setDisable(true);
        }
    }
    //mostrar per consola per comprovar
    @FXML
    public void btnSelecCertOnAction (ActionEvent event) {
        certificatHisendaPath=getTheUserFilePath();
        System.out.println("Directori certificat hisenda: " +  certificatHisendaPath);
        System.out.println("Data certificat hisenda: " +  dpCertificat.getValue());
    }

    @FXML
    public void btnSelecCertPRLOnAction (ActionEvent event) {
        certificatPRLPath=getTheUserFilePath();
        System.out.println("Directori certificat P.R.L: " +  certificatPRLPath);
        System.out.println("Data certificat P.R.L: " +  dpPRL.getValue());

    }
    @FXML
    public void btnSelecAssegMercOnAction (ActionEvent event) {
        assegurancaMercadeiraPath=getTheUserFilePath();
        System.out.println("Directori Assegurança mercadeira: " +  assegurancaMercadeiraPath);
        System.out.println("Data Assegurança mercadeira: " +  dpAssegMerc.getValue());

    }
    @FXML
    public void btnSelecAssegRCOnAction (ActionEvent event) {
        assegurancaRCPath=getTheUserFilePath();
        System.out.println("Directori Assegurança R.C: " +  assegurancaRCPath);
        System.out.println("Data Assegurança R.C: " +  dpAssegRC.getValue());

    }
    @FXML
    public void btnSelecTradeOnAction (ActionEvent event) {
        tradePath=getTheUserFilePath();
        System.out.println("Directori Trade: " +  tradePath);
    }

    public String getTheUserFilePath() {
        Stage stage = (Stage) anchorPaneAlta.getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Selecciona el document");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("ALL FILES", "*.*"),
                new FileChooser.ExtensionFilter("ZIP", "*.zip"),
                new FileChooser.ExtensionFilter("PDF", "*.pdf"),
                new FileChooser.ExtensionFilter("TEXT", "*.txt"),
                new FileChooser.ExtensionFilter("IMAGE FILES", "*.jpg", "*.png", "*.gif")
        );

        File file = fileChooser.showOpenDialog(stage.getScene().getWindow());

        if (file != null) {
            return file.toString();
        } else  {
            System.out.println("error");
        }

        return null;
    }

    public void btnTancaOnAction (ActionEvent event) {
        Stage stage = (Stage)btnX.getScene().getWindow();
        stage.close();
    }
    // <editor-fold defaultstate="collapsed" desc="Alertes">

    /**
     * Crea una alerta de tipus WARNING
     *  @param TitleText   el text que sortirà al títol de la alerta
     * @param HeaderText  el text que sortirà al header de la alerta
     */
    private void mostraAlertaWarning(String TitleText, String HeaderText) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(TitleText);
        alert.setHeaderText(HeaderText);
        alert.setContentText("Torna a intentar-ho");
        alert.showAndWait();
    }

    /**
     * Crea una alerta de tipus INFORMATION
     *
     */
    private void mostraAlertaInformation() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Fet");
        alert.setHeaderText("");
        alert.setContentText("Alta correcta");
        alert.showAndWait();
    }
    // </editor-fold>
}
