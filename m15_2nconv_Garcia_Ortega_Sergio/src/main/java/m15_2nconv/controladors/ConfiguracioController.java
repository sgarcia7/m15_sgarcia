package m15_2nconv.controladors;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import m15_2nconv.classes.DefaultControllerTransportista;


public class ConfiguracioController extends DefaultControllerTransportista {
    @FXML
    private Button btnSave;
    @FXML
    private Button btnX;
    @FXML
    private TextField tfResponsable1;
    @FXML
    private TextField tfResponsable2;
    @FXML
    private TextArea taMissatgeAdvertencia;
    @FXML
    private TextField tfCorreuAdministracio;
    @FXML
    private TextField tfDirectoriDocuments;

    public void btnTancaOnAction (ActionEvent event) {
        Stage stage = (Stage)btnX.getScene().getWindow();
        stage.close();
    }
}
