package m15_2nconv.controladors;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import m15_2nconv.classes.DefaultControllerTransportista;

public class SobreController extends DefaultControllerTransportista {

    @FXML
    Button btnTanca;

    public void btnTancaOnAction (ActionEvent event) {
        Stage stage = (Stage)btnTanca.getScene().getWindow();
        stage.close();
    }
}

