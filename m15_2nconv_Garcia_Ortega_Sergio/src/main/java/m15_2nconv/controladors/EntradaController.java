package m15_2nconv.controladors;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import m15_2nconv.classes.DefaultControllerTransportista;
import java.net.URL;
import java.util.ResourceBundle;

public class EntradaController extends DefaultControllerTransportista implements Initializable {
        @FXML
        AnchorPane rootPane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    new SplashScreen().start();
    }
    class SplashScreen extends Thread{
        public void run(){
            try {
                Thread.sleep(3000);
                System.out.println();
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {

                        rootPane.getScene().getWindow().hide();
                        obrir_vista("/view/Main.fxml",getEmf(),getConfig());

                    }

                });
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void obtenirDades() {

    }
}
