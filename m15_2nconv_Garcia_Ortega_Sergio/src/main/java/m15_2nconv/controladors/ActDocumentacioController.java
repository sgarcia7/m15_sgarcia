package m15_2nconv.controladors;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import m15_2nconv.classes.DefaultControllerTransportista;

public class ActDocumentacioController extends DefaultControllerTransportista {
    @FXML
    private TextArea taDetall;
    @FXML
    private Button btnTanca;
    @FXML
    private Button btnEnviar;
    @FXML
    private ProgressBar pbProgres;

    public void btnTancaOnAction (ActionEvent event) {
        Stage stage = (Stage)btnTanca.getScene().getWindow();
        stage.close();
    }
}
