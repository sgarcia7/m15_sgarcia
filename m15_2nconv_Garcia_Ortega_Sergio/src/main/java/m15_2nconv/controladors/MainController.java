package m15_2nconv.controladors;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.util.Duration;
import m15_2nconv.classes.DefaultControllerTransportista;

import java.time.LocalTime;
import java.util.Optional;

public class MainController extends DefaultControllerTransportista {
    @FXML
    private Label lbHora;

    @FXML
    public void initialize() {

        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            LocalTime currentTime = LocalTime.now();
            lbHora.setText(currentTime.getHour() + ":" + currentTime.getMinute() + ":" + currentTime.getSecond());
        }),
                new KeyFrame(Duration.seconds(1))
        );
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();

//        System.out.println("TEST: " + getEmf().toString() );

    }
    @FXML
    public void miSobreOnAction(ActionEvent event) {
        obrir_vista("/view/Sobre.fxml",getEmf(),getConfig());
    }
    @FXML
    public void miConfiguracioOnAction(ActionEvent event) {
        obrir_vista("/view/Configuracio.fxml",getEmf(),getConfig());

    }
    @FXML
    public void miAltaOnAction(ActionEvent event) {
        obrir_vista("/view/AltaTransportista.fxml",getEmf(),getConfig());

    }
    @FXML
    public void miBaixaTransportista(ActionEvent event) {
        obrir_vista("/view/BaixaTransportista.fxml",getEmf(),getConfig());

    }
    @FXML
    public void miCercaTransportista(ActionEvent event) {
        obrir_vista("/view/CercaTransportista.fxml",getEmf(),getConfig());

    }
    @FXML
    public void miActDocumentacio(ActionEvent event) {
        obrir_vista("/view/ActDocumentacio.fxml",getEmf(),getConfig());

    }
    @FXML
    public void miSurtOnAction(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setContentText("Estàs segur de sortir?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            Platform.exit();
        }
    }

    @Override
    public void obtenirDades() {

    }

}
