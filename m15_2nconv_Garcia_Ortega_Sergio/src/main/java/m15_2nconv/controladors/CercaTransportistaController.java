package m15_2nconv.controladors;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import m15_2nconv.classes.DAOHelper;
import m15_2nconv.classes.DefaultControllerTransportista;
import m15_2nconv.interfaces.IniciDiferit;
import m15_2nconv.models.DadesTransportista;
import java.net.URL;
import java.util.ResourceBundle;

public class CercaTransportistaController extends DefaultControllerTransportista implements Initializable, IniciDiferit {
    @FXML
    private TextField tfBuscar;
    @FXML
    private TableView<DadesTransportista> tvTransportistas;
    @FXML
    private TableColumn<DadesTransportista, String> nomColumn;
    @FXML
    private TableColumn<DadesTransportista, String> CIFColumn;
    @FXML
    private TableColumn<DadesTransportista, String> emailColumn;
    @FXML
    private TableColumn<DadesTransportista, String> idColumn;

    private static final int DOBLE_CLICK = 2;
    private static final int FIRST_ITEM = 0;
    private DAOHelper<DadesTransportista> helper;
    private ObservableList<DadesTransportista> llistaTransportistas;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ContextMenu cm = new ContextMenu();
        MenuItem mi1 = new MenuItem("Actualitza");

        cm.getItems().add(mi1);

        mi1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                actualitzarDades((DadesTransportista) tvTransportistas.getSelectionModel().getSelectedItem());
            }
        });

        tvTransportistas.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getButton() == MouseButton.SECONDARY) {
                    cm.show(tvTransportistas, event.getScreenX(), event.getScreenY());
                }
            }
        });

        configuraColumnes();
    }

        private void actualitzarDades(DadesTransportista transportista) {
            obrir_vistaActualitzar("/view/ActDocumentacio.fxml", getEmf(),transportista.getTransportista(),getConfig());
        }

        @Override
        public void iniciDiferit() {
            helper = new DAOHelper<>(getEmf(), DadesTransportista.class);
            actualitzaTaula();
            goTableItem(FIRST_ITEM);
        }

        private void actualitzaTaula() {
            llistaTransportistas = getTransportistes();
            tvTransportistas.setItems(llistaTransportistas);
            filtraTaula();
        }

        private void filtraTaula() {
            FilteredList<DadesTransportista> filteredData = new FilteredList<>(tvTransportistas.getItems(), p -> true);
            // 2. Set the filter Predicate whenever the filter changes.
            tfBuscar.textProperty().addListener((observable, oldValue, newValue) -> {
                filteredData.setPredicate(transportista -> {
                    // If filter text is empty, display all persons.
                    if (newValue == null || newValue.isEmpty()) {
                        return true;
                    }

                    // Compare first name and last name of every person with filter text.
                    String lowerCaseFilter = newValue.toLowerCase();

                    if (transportista.getNom().toLowerCase().contains(lowerCaseFilter)) {
                        return true; // Filter matches first name.
                    } else if (transportista.getCifnif().toLowerCase().contains(lowerCaseFilter)) {
                        return true; // Filter matches Cifnif.
                    } else if (transportista.getEmail().toLowerCase().contains(lowerCaseFilter)) {
                        return true; // Filter matches Cifnif.
                    }
                    return false; // Does not match.
                });
            });

            // 3. Wrap the FilteredList in a SortedList.
            SortedList<DadesTransportista> sortedData = new SortedList<>(filteredData);

            // 4. Bind the SortedList comparator to the TableView comparator.
            sortedData.comparatorProperty().bind(tvTransportistas.comparatorProperty());

            // 5. Add sorted (and filtered) data to the table.
            tvTransportistas.setItems(sortedData);

        }

        private void configuraColumnes() {
            // Alerta que els valors --------------------------------<>
            // Identifiquen el noms de les VARIABLES de la classe
            idColumn.setCellValueFactory(new PropertyValueFactory<DadesTransportista, String>("idTransportista"));
            nomColumn.setCellValueFactory(new PropertyValueFactory<DadesTransportista, String>("nom"));
            CIFColumn.setCellValueFactory(new PropertyValueFactory<DadesTransportista, String>("cifnif"));
            emailColumn.setCellValueFactory(new PropertyValueFactory<DadesTransportista, String>("email"));

        }

        private ObservableList<DadesTransportista> getTransportistes() {
            return FXCollections.observableArrayList(helper.getAll());
        }

        private void goTableItem(int row) {
            tvTransportistas.requestFocus();
            tvTransportistas.scrollTo(row);
            tvTransportistas.getSelectionModel().select(row);
            tvTransportistas.getFocusModel().focus(row);
        }









    }


