package m15_2nconv.controladors;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import m15_2nconv.classes.DAOHelper;
import m15_2nconv.classes.DefaultControllerTransportista;
import m15_2nconv.interfaces.IniciDiferit;
import m15_2nconv.models.DadesTransportista;
import m15_2nconv.models.Transportista;

import java.net.URL;
import java.util.ResourceBundle;

public class BaixaTransportistaController extends DefaultControllerTransportista implements Initializable, IniciDiferit {
    @FXML
    private TextField tfBuscar;
    @FXML
    private TableView<DadesTransportista> tvTransportistas;
    @FXML
    private TableColumn<DadesTransportista, String> nomColumn;
    @FXML
    private TableColumn<DadesTransportista, String> CIFColumn;
    @FXML
    private TableColumn<DadesTransportista, String> emailColumn;
    @FXML
    private TableColumn<DadesTransportista, Integer> idColumn;
    @FXML
    private TableColumn<DadesTransportista, Boolean> baixaColumn;

    private static final int DOBLE_CLICK = 2;
    private static final int FIRST_ITEM = 0;
    private DAOHelper<DadesTransportista> helper;
    private DAOHelper<Transportista> helper2;

    private ObservableList<DadesTransportista> llistaTransportistas;
    DadesTransportista dadesTransportista = new DadesTransportista();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ContextMenu cm = new ContextMenu();
        MenuItem mi1 = new MenuItem("Baixa Temporal");
        MenuItem mi2 = new MenuItem("Baixa Completa");
        MenuItem mi3 = new MenuItem("Donar d'alta");

        cm.getItems().add(mi1);
        cm.getItems().add(mi2);
        cm.getItems().add(mi3);

        mi1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                baixaTemporal((DadesTransportista) tvTransportistas.getSelectionModel().getSelectedItem());
                actualitzaTaula();
            }
        });
        mi2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               //baixaCompleta((DadesTransportista) tvTransportistas.getSelectionModel().getSelectedItem());
                //helper.deleteById(String.valueOf( tvTransportistas.getSelectionModel().getSelectedItem().getIdTransportista()));
                baixaCompleta(tvTransportistas.getSelectionModel().getSelectedItem().getTransportista());
                actualitzaTaula();
            }
        });
        mi3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                altaTemporal((DadesTransportista) tvTransportistas.getSelectionModel().getSelectedItem());
                actualitzaTaula();
            }
        });
        tvTransportistas.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getButton() == MouseButton.SECONDARY) {
                    cm.show(tvTransportistas, event.getScreenX(), event.getScreenY());
                }
            }
        });

        configuraColumnes();
    }

    private void baixaTemporal(DadesTransportista transportista) {
        //transportista.getTransportista().getDadesTransportista().setBaixa("TEMPORAL");

        transportista.setBaixa(true);
        helper.update(transportista);
        mostraAlertaInformation("Baixa temporal correcte");
    }
    private void altaTemporal(DadesTransportista transportista) {
        //transportista.getTransportista().getDadesTransportista().setBaixa("TEMPORAL");
        transportista.setBaixa(false);
        helper.update(transportista);
        mostraAlertaInformation("Alta correcte");
    }
    private void baixaCompleta(Transportista transportista) {
        //helper.delete(transportista.getIdTransportista());
        //helper2.delete(transportista);
        //helper.delete(transportista);
        //helper.deleteTransportista(transportista);
        helper.deleteTest(transportista);
        System.out.println(transportista);
        mostraAlertaInformation("Baixa correcte");

    }

    @Override
    public void iniciDiferit() {
        helper = new DAOHelper<>(getEmf(), DadesTransportista.class);
        helper2 = new DAOHelper<>(getEmf(),Transportista.class);
        actualitzaTaula();
        goTableItem(FIRST_ITEM);
    }

    private void actualitzaTaula() {
        llistaTransportistas = getTransportistes();
        tvTransportistas.setItems(llistaTransportistas);
        filtraTaula();
    }

    private void filtraTaula() {
        FilteredList<DadesTransportista> filteredData = new FilteredList<>(tvTransportistas.getItems(), p -> true);
        // 2. Set the filter Predicate whenever the filter changes.
        tfBuscar.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(transportista -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (transportista.getNom().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (transportista.getCifnif().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches Cifnif.
                }
                return false; // Does not match.
            });
        });

        // 3. Wrap the FilteredList in a SortedList.
        SortedList<DadesTransportista> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(tvTransportistas.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        tvTransportistas.setItems(sortedData);

    }
    private void configuraColumnes() {
        // Alerta que els valors --------------------------------<>
        // Identifiquen el noms de les VARIABLES de la classe
        idColumn.setCellValueFactory(new PropertyValueFactory<DadesTransportista, Integer>("idTransportista"));
        nomColumn.setCellValueFactory(new PropertyValueFactory<DadesTransportista, String>("nom"));
        CIFColumn.setCellValueFactory(new PropertyValueFactory<DadesTransportista, String>("cifnif"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<DadesTransportista, String>("email"));
        // Per mostrar un CheckBox en un camp Boolean
        baixaColumn.setCellValueFactory(new PropertyValueFactory<DadesTransportista, Boolean>("baixa"));
        baixaColumn.setCellFactory(column -> new CheckBoxTableCell<>());
        baixaColumn.setCellValueFactory(cellData -> cellData.getValue().getBixaProperty());
    }

    private ObservableList<DadesTransportista> getTransportistes() {
        return FXCollections.observableArrayList(helper.getAll());
    }

    private void goTableItem(int row) {
        tvTransportistas.requestFocus();
        tvTransportistas.scrollTo(row);
        tvTransportistas.getSelectionModel().select(row);
        tvTransportistas.getFocusModel().focus(row);
    }


    // <editor-fold defaultstate="collapsed" desc="Alertes">

    /**
     * Crea una alerta de tipus WARNING
     *  @param TitleText   el text que sortirà al títol de la alerta
     * @param HeaderText  el text que sortirà al header de la alerta
     */
    private void mostraAlertaWarning(String TitleText, String HeaderText) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(TitleText);
        alert.setHeaderText(HeaderText);
        alert.setContentText("Torna a intentar-ho");
        alert.showAndWait();
    }

    /**
     * Crea una alerta de tipus INFORMATION
     *
     */
    private void mostraAlertaInformation(String content) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Fet");
        alert.setHeaderText("");
        alert.setContentText(content);
        alert.showAndWait();
    }
    // </editor-fold>


}


