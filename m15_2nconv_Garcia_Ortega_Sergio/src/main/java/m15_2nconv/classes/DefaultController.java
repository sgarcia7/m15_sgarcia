package m15_2nconv.classes;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import m15_2nconv.interfaces.IniciDiferit;
import m15_2nconv.models.ConfiguracioSistema;
import m15_2nconv.models.Transportista;

import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class DefaultController implements IniciDiferit {

    private EntityManagerFactory emf;
    private ConfiguracioSistema config;

    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }
    public EntityManagerFactory getEmf() {
        return emf;
    }
    public ConfiguracioSistema getConfig() {
        return config;
    }
    public void setConfig(ConfiguracioSistema config) {
        this.config = config;
    }

    public void obrir_vista(String vista, EntityManagerFactory emf, ConfiguracioSistema config){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource(vista));

            Parent root1 = fxmlLoader.load();
            DefaultControllerTransportista controlador = fxmlLoader.getController();
            controlador.setEmf(emf);
            controlador.setConfig(config);
            controlador.obtenirDades();
            controlador.iniciDiferit();
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (IOException e) {
            //Missatge d'error al no poder obrir correctament la Stage
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }
    }
    public void obrir_vistaActualitzar(String vista, EntityManagerFactory emf, Transportista transportista, ConfiguracioSistema config){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource(vista));

            Parent root1 = fxmlLoader.load();
            DefaultControllerTransportista controlador = fxmlLoader.getController();
            controlador.setEmf(emf);
            controlador.setTransportista(transportista);
            controlador.setConfig(config);
            controlador.obtenirDades();
            controlador.iniciDiferit();
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("Transportista - " + transportista.getDadesTransportista().getNom());
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (IOException e) {
            //Missatge d'error al no poder obrir correctament la Stage
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", e);
        }
    }
    @Override
    public void iniciDiferit() {

    }

}
