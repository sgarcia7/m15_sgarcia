package m15_2nconv.classes;


import m15_2nconv.interfaces.Dades;
import m15_2nconv.interfaces.IniciDiferit;
import m15_2nconv.models.Transportista;

public class DefaultControllerTransportista extends DefaultController implements Dades, IniciDiferit {

    private Transportista transportista;
    public void setTransportista(Transportista transportista) {
        this.transportista = transportista;
    }
    public Transportista getTransportista() {
        return transportista;
    }

    @Override
    public void obtenirDades() {
    }

    @Override
    public void iniciDiferit() {

    }
}
