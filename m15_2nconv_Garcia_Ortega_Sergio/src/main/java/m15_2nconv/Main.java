package m15_2nconv;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import m15_2nconv.classes.AppPropertiesHelper;
import m15_2nconv.classes.DefaultController;
import m15_2nconv.exceptions.PropertiesHelperException;
import m15_2nconv.models.ConfiguracioSistema;
import m15_2nconv.models.Transportista;
import org.apache.commons.configuration.ConfigurationException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends Application {
    private EntityManagerFactory emf;
    private ConfiguracioSistema config;

    @Override
    public void start(Stage primaryStage){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Entrada.fxml"));
        Parent root;
        //Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/view/Entrada.fxml")));
        try {
            root = loader.load();
            DefaultController controller = loader.getController();
            controller.setEmf(emf);
            controller.setConfig(config);
            Scene scene = new Scene(root, 350, 400);
            primaryStage.initStyle(StageStyle.UNDECORATED);
            primaryStage.setScene(scene);
            primaryStage.show();
            primaryStage.setResizable(false);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            Platform.exit();
        }

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);

    }

    /**
     * El següent mètode inicialitza la connexió per poder accedir a la base de dades i carregará
     * la configuracio
     * @throws Exception
     */
    @Override
    public void init() throws Exception {
        super.init();
        String passwd = getDecryptedPropsFilePassword();
        Map<String,String> mapprop = new HashMap<>();
        mapprop.put("javax.persistence.jdbc.password",passwd);
              try{
                  emf = Persistence.createEntityManagerFactory( "mariaDBConnection",mapprop);
            EntityManager em = emf.createEntityManager();
                  config = new ConfiguracioSistema();
                  config = em.find(config.getClass(),1);
                  System.out.println("TEST: " + em.toString() );
            em.close();
        }catch(Exception ignored){
        }
    }
    private static String getDecryptedPropsFilePassword() throws PropertiesHelperException, ConfigurationException {
        AppPropertiesHelper helper = new AppPropertiesHelper("app.properties", "password", "enc");
        return helper.getDecryptedUserPassword();
    }
    @Override
    public void stop() throws Exception {
        if (emf != null && emf.isOpen())
            emf.close();

        super.stop();
    }
}
